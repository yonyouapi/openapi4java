#openapi4java

- OpenAPI4Java SDK 为用友官方发布的 SDK，以最新更新为主。

### 版权
- 用友优普技术有限公司
- 用友优普开放平台

### 关于我们
- [用友优普开放平台官网](http://open.yonyouup.com)
- QQ 交流群：320582917
- 微信公众号<br/>
  ![微信公众号](http://open.yonyouup.com/styles/function/documentCenter/images/qaCode.png "微信公众号")


### 项目说明
- OpenAPI4JAVA sdk主项目
- OpenAPI4JAVA.src sdk源码
- OpenAPI4JAVA.Examples 测试示例
- 推荐使用eclipse作为IDE

### 准备工作
- 使用 SDK 前请先配置：<br/>
  resources/config.properties
- 配置JDK环境 需要JDK1.5及以上

### API 调用过程
1. 实例化 某API的service 类<br/>
   AccountService ds = new AccountService(); // 获取u8帐套api
2. 准备参数<br/>
   String to_account = args[0];           //提供方id    	 
   String id = args[1];					//帐套号	     
3. 调用api返回JSON格式的数据<br/>
   JSONObject record = ds.get(id, to_account);

