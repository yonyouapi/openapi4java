package openapi4j.platform;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import openapi4j.commons.TokenManager;
import openapi4j.exception.OpenAPIException;
import openapi4j.util.HttpUtil;
import openapi4j.util.PropUtil;


public class OrderstatusService {
	

	/**
	 * 获取订单状态
	 * @param to_account
	 * @return
	 * @throws OpenAPIException
	 */
	    public static String getOrderstatus(String to_account) throws OpenAPIException {
	    	Properties prop = PropUtil.getProperties("/config.properties");
	        String url = prop.getProperty("api_url_orderstatus_get");
	        String from_account = prop.getProperty("from_account");
	        String app_key = prop.getProperty("app_key");
	        String token = TokenManager.getToKenId();
	        url = StringUtils.replace(url, "{from_account}", from_account);
	        url = StringUtils.replace(url, "{to_account}", to_account);
	        url = StringUtils.replace(url, "{app_key}", app_key);
	        url = StringUtils.replace(url, "{token}", token);

	         String orderstatus = null;
			try {
				 orderstatus =  HttpUtil.get(url);
				 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new OpenAPIException(e.getMessage(),e);
			}
	       
	       
	        return orderstatus;
	    }
	    
	   

	   
}
