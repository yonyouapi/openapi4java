package openapi4j.platform;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;

import openapi4j.commons.TokenManager;
import openapi4j.exception.OpenAPIException;
import openapi4j.util.HttpUtil;
import openapi4j.util.PropUtil;


public class TradeService {
	

	/**
	 * 获取交易号
	 * @return
	 * @throws OpenAPIException
	 */
	    public static String getTradeId() throws OpenAPIException {
	    	Properties prop = PropUtil.getProperties("/config.properties");
	        String url = prop.getProperty("api_url_trade_get");
	        String from_account = prop.getProperty("from_account");
	        String app_key = prop.getProperty("app_key");
	        String token = TokenManager.getToKenId();
	        url = StringUtils.replace(url, "{from_account}", from_account);
	        url = StringUtils.replace(url, "{app_key}", app_key);
	        url = StringUtils.replace(url, "{token}", token);

	         String tradeId = null;
			try {
				String str =  HttpUtil.get(url);
				 JSONObject jsonObject = JSONObject.parseObject(str);
			     JSONObject tradeObject = jsonObject.getJSONObject("trade");
			     tradeId = tradeObject.getString("id");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new OpenAPIException(e.getMessage(),e);
			}
	       
	       
	        return tradeId;
	    }
	    
	   

	   
}
