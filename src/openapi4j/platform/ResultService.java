package openapi4j.platform;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import openapi4j.exception.OpenAPIException;
import openapi4j.util.HttpUtil;
import openapi4j.util.PropUtil;


public class ResultService {
	

	/**
	 * 异步新增查看新增结果
	 * @param requestid
	 * @return
	 * @throws OpenAPIException
	 */
	    public static String getResult(String requestid) throws OpenAPIException {
	    	Properties prop = PropUtil.getProperties("/config.properties");
	        String url = prop.getProperty("api_url_result_get");
	       
	        url = StringUtils.replace(url, "{requestid}", requestid);

	         String resultStr = null;
			try {
				resultStr =  HttpUtil.get(url);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new OpenAPIException(e.getMessage(),e);
			}
	       
	       
	        return resultStr;
	    }
	    
	   

	   
}
